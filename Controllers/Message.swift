//
//  Message.swift
//  cache.find
//
//  Created by Ivan Frias on 29/12/14.
//  Copyright (c) 2014 Emanuel Coelho. All rights reserved.
//

import Foundation

class Message {
    var origin:NSString
    var destination:NSString
    var messageType:NSString
    var title:NSString
    var description:NSString
    var createdAt:NSString
    
    init(fromOrigin origin: NSString,fromDestination destination: NSString, fromMessageType messageType: NSString, fromTitle title: NSString, fromDescription description: NSString, fromCreatedAt createdAt: NSString){
        self.origin = origin
        self.destination = destination
        self.messageType = messageType
        self.title = title
        self.description = description
        self.createdAt = createdAt
    }
}