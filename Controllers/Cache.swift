//
//  Cache.swift
//  cache.find
//
//  Created by Ivan Frias on 29/12/14.
//  Copyright (c) 2014 Emanuel Coelho. All rights reserved.
//

import Foundation

class ChallengeCache {
    var description:NSString
    var name:NSString
    var status:NSString
    var terrain:NSString
    
    init (fromDescription: NSString, fromName: NSString, fromStatus:NSString, fromTerrain:NSString){
        self.description = fromDescription
        self.name = fromName
        self.status = fromStatus
        self.terrain = fromTerrain
    }
    
}
